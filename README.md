# 《软件需求分析》

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [关于本仓库](#关于本仓库)
    1. [本仓库许可协议](#本仓库许可协议)
    2. [本仓库编写环境和使用方法](#本仓库编写环境和使用方法)
    3. [帮助完善本仓库](#帮助完善本仓库)
    4. [本仓库组织](#本仓库组织)
2. [关于本课程](#关于本课程)
    1. [课程基本信息](#课程基本信息)
    2. [课程教材与实验教材](#课程教材与实验教材)
3. [参考资料](#参考资料)
    1. [专著](#专著)
    2. [其他](#其他)

<!-- /code_chunk_output -->

## 关于本仓库

### 本仓库许可协议

本仓库使用`CC-BY-SA-4.0`协议，更详细的协议内容请见<https://gitlab.com/arm_commons/commons/-/blob/master/LICENSES/CC-BY-SA-4.0/README.md>

### 本仓库编写环境和使用方法

请详见<https://gitlab.com/arm_commons/commons/-/blob/master/ENVS/MD_RAW/README.md>

### 帮助完善本仓库

期待您一起完善本仓库，您可以通过以下方式帮助完善本仓库：

1. **`merge request`**： 通过`GitLab`的`merge request`到`master`分支
1. **`issue`**： 发起一个新的`issue`（标签设置成`optimize`）

### 本仓库组织

1. 目录组织
    1. **`Addons/`** ： 附加文档，如：专题类的论述
    1. **`Coursewares/`** ： 使用`markdown`制作的课件
    1. **`Experiments/`** ： 实验指导文档
    1. **`Extras/`** ： 教材之外的内容
    1. **`Outlines/`** ： 各部分的大纲
    1. **`Homeworks/`** ： 课后习题
    1. **`README.md`** ： 本文件
1. 章节对应
    1. **`Part_00`** 或 **`pt00`** ： Preface导言
    1. **`Part_01`** 或 **`pt01`** ： Software Requirements: What, Why, and Who软件需求的3W（什么、为什么和谁）
    1. **`Part_02`** 或 **`pt02`** ： Requirements Development需求开发
    1. **`Part_03`** 或 **`pt03`** ： Requirements for Specific Project Classes具体项目类别的需求
    1. **`Part_04`** 或 **`pt04`** ： Requirements Management需求管理
    1. **`Part_05`** 或 **`pt05`** ： Implementing Requirements Engineering需求工程的实施

## 关于本课程

### 课程基本信息

1. 课程名称： 《软件需求分析》（_Software Requirements Analysis_）
1. 课程类别： 专业必修课
1. 先修课程： 《软件工程导论》以及至少一门编程设计语言课程

### 课程教材与实验教材

1. :book: [《软件需求》（第3版）](http://www.tup.tsinghua.edu.cn/booksCenter/book_05387201.html)，[美]Karl Wiegers, Joy Beatty著，李忠利 李淳 霍金健 孔晨辉译，清华大学出版社，2016-02，ISBN：9787302426820，[豆瓣链接](https://book.douban.com/subject/26307910/)
1. :book: 《软件需求分析师岗位指导教程》，桂超主编、孔敏丛书主编，南京大学出版社，2017-01，ISBN：9787305145155

## 参考资料

### 专著

1. :book: 《大象：Thinking in UML（第2版）》，谭云杰 著，中国水利水电出版社，2012-03，ISBN：9787508492346
    1. :computer: [大象-Thinking in UML（第二版）-配套资料-谭云杰（2012-03-14）](http://waterpub.com.cn/softdown/FileInfo-1.Asp?SoftID=3584)
    1. :computer: [大象-Thinking in UML（第二版）](http://www.wsbookshow.com/bookshow/jc/yjs/qtl/11383.html)
    1. :computer: [coffeewoo的专栏：Thinking in UML](https://blog.csdn.net/coffeewoo)
    1. :computer: [coffeewoo itpub](http://coffeewoo.itpub.net/)（可能已失效）

### 其他
