# SR-H01

## Questions

1. 需求开发的结果将写入（       ）  
A. Report of Feasibility Study  
B. Vision and Scope Document  
C. User Requirement Specifications  
D. Software Requirement Specifications  

1. 需求通常分为三个层次,即业务需求、用户需求和（        ）  
A. 功能需求  
B. 非功能需求  
C. 数据需求  
D. 软件需求  

1. 当用户无法完成主动的信息告知，或用户与需求工程师之间的语言交流无法产生有效的结果时，有必要采用（   ）  
A. Interviews  
B. Workshops  
C. Observations  
D. Questionnaires

1. 描述场景所使用的表示法要符合正规性要求,一般可使用自然语言、可视化模型、半形式化语言和形式化语言，在实践中，以下是最主要的描述方法（          ）  
A. 自然语言  
B. 可视化模型  
C. 半形式化语言  
D. 形式化语言

1. 描述了系统的输入、处理、存储和输出，以及它们如何在一起协调工作的可视化模型是（       ）  
A. DFD  
B. ERD  
C. STD  
D. UCD

## Keys

1. D, :book:P9
1. A, :book:P7图1-1
1. C, :book:P111
1. A, :book:P187
1. A, :book:P202图12-1
