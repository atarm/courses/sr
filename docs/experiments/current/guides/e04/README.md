# 需求分析与需求建模

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [实验子项01实验指导](#实验子项01实验指导)
    1. [实验子项01实验内容](#实验子项01实验内容)
    2. [实验子项01实验参考资料](#实验子项01实验参考资料)
    3. [实验子项01实验参考结果](#实验子项01实验参考结果)
2. [实验子项02实验指导](#实验子项02实验指导)
    1. [实验子项02实验内容](#实验子项02实验内容)
    2. [实验子项02实验参考资料](#实验子项02实验参考资料)
    3. [实验子项02实验参考结果](#实验子项02实验参考结果)
3. [实验子项03实验指导](#实验子项03实验指导)
    1. [实验子项03实验内容](#实验子项03实验内容)
    2. [实验子项03实验参考资料](#实验子项03实验参考资料)
    3. [实验子项03实验参考结果](#实验子项03实验参考结果)

<!-- /code_chunk_output -->

## 实验子项01实验指导

### 实验子项01实验内容

根据"食堂订餐系统（COS）"获取用户需求并建立的用例图模型，使用UML活动图技术建立"用例规约-订餐"的活动图模型

### 实验子项01实验参考资料

1. :book:教材《软件需求（第3版）》**P512"UC-1：订餐"**
1. :bookmark: **[activity_diagram.md](../../../Extras/02_PlantUML/03_Activity_Diagram/activity_diagram.md)** 的内容

### 实验子项01实验参考结果

@import "../../Expt_Anses/SR-E04_Ans/00_Diagram/01_COS_Order_AD_01.puml"

@import "../../Expt_Anses/SR-E04_Ans/00_Diagram/01_COS_Order_AD_02_E01.puml"

@import "../../Expt_Anses/SR-E04_Ans/00_Diagram/01_COS_Order_AD_03_E02.puml"

@import "../../Expt_Anses/SR-E04_Ans/00_Diagram/01_COS_Order_AD_04_S01.puml"

@import "../../Expt_Anses/SR-E04_Ans/00_Diagram/01_COS_Order_AD_04_S02.puml"

## 实验子项02实验指导

### 实验子项02实验内容

根据"食堂订餐系统（COS）"的"订餐订单"状态以及所允许的状态变换，利用UML状态图技术建立COS的"订餐订单"STD图

![httpatomoreillycomsourcemspimages1792392](./00_Image/httpatomoreillycomsourcemspimages1792392.png)

### 实验子项02实验参考资料

1. :book:教材《软件需求（第3版）》**P523"图C-4 订餐订单的状态转化图"（:exclamation::exclamation::exclamation:教材中该图有误，请参考勘误或英文原图）**
1. :bookmark: **[state_diagram.md](../../../Extras/02_PlantUML/04_State_Diagram/state_diagram.md)** 的内容

### 实验子项02实验参考结果

@import "../../Expt_Anses/SR-E04_Ans/00_Diagram/02_COS_Order_SD.puml"

## 实验子项03实验指导

### 实验子项03实验内容

根据"食堂订餐系统（COS）"的"局部数据模型"，使用ERD的UML符号法建立ERD数据模型

![httpatomoreillycomsourcemspimages1792391](./00_Image/httpatomoreillycomsourcemspimages1792391.png)

### 实验子项03实验参考资料

1. :book:教材《软件需求（第3版）》**P518"图C-3 食堂订餐系统版本1.0的局部数据模型"**
1. :bookmark:  **[entity-relationship_diagram.md](../../../Extras/02_PlantUML/06_Entity-Relationship_Diagram/entity-relationship_diagram.md)** 的内容

### 实验子项03实验参考结果

@import "../../Expt_Anses/SR-E04_Ans/00_Diagram/03_COS_ERD.puml"
