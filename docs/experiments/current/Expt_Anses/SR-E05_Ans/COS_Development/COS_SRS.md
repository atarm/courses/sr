# COS软件需求规格说明书（软件需求规范）

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [介绍Introduction](#介绍introduction)
    1. [目的Purpose](#目的purpose)
    2. [文档约定Document Conventions](#文档约定document-conventions)
    3. [项目范围Project Scope](#项目范围project-scope)
    4. [参考References](#参考references)
2. [总述Overall Description](#总述overall-description)
    1. [产品视角Product Perspective](#产品视角product-perspective)
    2. [用户群及特征User Classes and Characteristics](#用户群及特征user-classes-and-characteristics)
    3. [操作环境Operating Environment](#操作环境operating-environment)
    4. [设计与实现约束Design and Implementation Constraints](#设计与实现约束design-and-implementation-constraints)
    5. [假设与依赖Assumptions and Dependencies](#假设与依赖assumptions-and-dependencies)
3. [系统特性System Features](#系统特性system-features)
    1. [3.1 从食堂订餐Order Meals from Cafeteria](#31-从食堂订餐order-meals-from-cafeteria)
        1. [描述Description](#描述description)
        2. [功能性需求Functional Requirements](#功能性需求functional-requirements)
    2. [从饭馆订餐Order Meals from Restaurants](#从饭馆订餐order-meals-from-restaurants)
    3. [创建、查看、修改以及删除已订的菜品Create, View, Modify, and Delete Meal Subscriptions](#创建-查看-修改以及删除已订的菜品create-view-modify-and-delete-meal-subscriptions)
    4. [创建、查看、修改以及删除食堂菜单Create, View, Modify, and Delete Cafeteria Menus](#创建-查看-修改以及删除食堂菜单create-view-modify-and-delete-cafeteria-menus)
4. [数据需求Data Requirements](#数据需求data-requirements)
    1. [逻辑数据模型Logical Data Model](#逻辑数据模型logical-data-model)
    2. [数据字典Data Dictionary](#数据字典data-dictionary)
    3. [报表Reports](#报表reports)
        1. [订餐历史报表Ordered Meal History Report](#订餐历史报表ordered-meal-history-report)
    4. [数据集成、留存和销毁Data Integrity, Retention, and Disposal](#数据集成-留存和销毁data-integrity-retention-and-disposal)
5. [外部接口需求External Interface Requirements](#外部接口需求external-interface-requirements)
    1. [用户界面User Interfaces](#用户界面user-interfaces)
    2. [软件接口Software Interfaces](#软件接口software-interfaces)
    3. [硬件接口Hardware Interfaces](#硬件接口hardware-interfaces)
    4. [通信接口Communications Interfaces](#通信接口communications-interfaces)
6. [质量属性Quality Attributes](#质量属性quality-attributes)
    1. [易用性需求Usability Requirements](#易用性需求usability-requirements)
    2. [性能需求Performance Requirements](#性能需求performance-requirements)
    3. [保密性需求Security Requirements](#保密性需求security-requirements)
    4. [安全性需求Safety Requirements](#安全性需求safety-requirements)
    5. [可用性需求Availability Requirements](#可用性需求availability-requirements)
    6. [健壮性需求Robustness Requirements](#健壮性需求robustness-requirements)
7. [附录A：分析模型Analysis Models](#附录a分析模型analysis-models)

<!-- /code_chunk_output -->

## 介绍Introduction

### 目的Purpose

本软件需求规范描述了食堂订餐系统（COS）1.0版本软件中的功能性和非功能性需求。此文档由项目团队成员使用，以实现并检验正确的系统功能。除非另有说明，否则1.0版本中承诺包含这里所规范的所有需求。

### 文档约定Document Conventions

本SRS中未使用特定书面约定。

### 项目范围Project Scope

COS系统将允许Process Impact公司员工在线从公司食堂订餐并送到园区指定的位置。在[《食堂订餐系统愿景与范围文档》](#REF-COS_VSD)中有详尽的描述，并且阐述了在此版本中规划的需要完整或部分实现的特性。

### 参考References

1. <a id="REF-COS_VSD">《食堂订餐系统愿景与范围文档》</a>，Wiegers、Karl，www.processimpact.com/projects/COS/COS%20Vision%20and%20Scope.docx
1. <a id="REF-PI_IDS">《Process Impact公司内网开发标准（1.3版）》</a>，Beatty，www.processimpact.com/corporate/standards/PI%20Intranet%20Development%20Standard.pdf
1. <a id="REF-PI_IUIS">《Process Impact公司互联网应用用户界面标准（2.0版）》</a>，Rath、Andrew，www.processimpact.com/corporate/standards/PI%20Internet%20UI%20Standard.pdf

## 总述Overall Description

### 产品视角Product Perspective

食堂订餐系统是一套新的软件系统，用于替代Process Impact食堂现在通过人工和电话处理订餐到取餐的方式。图C-2中的环境图绘制了1.0版本的外部实体和系统接口。预期系统将经过多个版本的进化，并且最终为一些当地餐馆连接到互联网订餐服务以及支持储蓄卡和信用卡的鉴权服务。

### 用户群及特征User Classes and Characteristics

|用户群|描述|
|--|--|
|顾客（首要）|顾客是希望从公司食堂进行订外卖的Process Impact公司员工。潜在顾客大约有600名，预计其中300人平均每周每人使用COS系统5次。有时，顾客会为团队会议或访客预订多份。估计60%的订餐由公司内网下单，40%的订餐从家中或使用智能电话和平板电脑下单|
|食堂工作人员|Process Impact公司食堂雇佣了约20名食堂工作人员，他们将从COS系统接收订单、备餐，为外卖打包并请求送餐。大多数食堂工作人员都需要接受培训，学习如何使用COS系统的软硬件|
|菜单管理员|菜单管理员是一名食堂雇员，负责创建和维护每天食堂可供应菜品的菜单。有些菜单项可能无法外送。菜单管理员还会定义每天的特色菜。菜单管理员需要定期编辑已有的菜单|
|送餐人员|当食堂工作人员准备外卖订单时，他们会向送餐人员的智能手机发送送餐请求。送餐人员提取订餐并送至顾客。送餐人员与COS系统的其他交互包括确认订餐是否送达|

### 操作环境Operating Environment

1. OE-1：OCS系统将可以在如下网页浏览器中正常使用：Windows Internet Explorer版本7、8、9；Firefox版本12至26；Google Chrome全部版本；Apple Safari4.0至8.0<!--软件环境一般向后兼容，因此，一般描述最低版本，特殊情况下，列明不兼容的版本-->
1. OE-2：COS系统闺怨运行于当前企业审批通过版本的Redhat Linux服务器和Apache HTTP服务器
1. OE-3：COS系统将允许用户从企业内网、VPN互联网连接或使用Android、IOS、Windows智能手机和平板电脑进行访问

### 设计与实现约束Design and Implementation Constraints

1. CO-1： 系统的设计、代码、维护文档应遵循[《影响力过程内网开发标准（1.3版）》](#REF-PI_IDS)
1. CO-2： 系统将使用当前企业标准Oracle数据库引擎
1. CO-3： 所有HTML代码应遵循HTML5.0标准

### 假设与依赖Assumptions and Dependencies

1. AS-1： 认为员工在工作日在线，因此食堂会在每个工作日开放早、中、晚三餐
1. DE-1： COS系统的运行依赖于为了薪资系统能够接受COS系统的订单支付请求而做的修改
1. DE-2： COS系统的运行依赖于为了在COS系统接受订餐时更新菜品项的有效性而对食堂库存系统所做的修改

## 系统特性System Features

### 3.1 从食堂订餐Order Meals from Cafeteria

#### 描述Description

身份经过验证的食堂顾客能够订餐，并要求送餐至指定的公司位置或到食堂自取。顾客如果还没有准备好，可以取消或修改订餐订单。

优先级=高。

#### 功能性需求Functional Requirements

（略）

### 从饭馆订餐Order Meals from Restaurants

（略）

### 创建、查看、修改以及删除已订的菜品Create, View, Modify, and Delete Meal Subscriptions

（略）

### 创建、查看、修改以及删除食堂菜单Create, View, Modify, and Delete Cafeteria Menus

（略）

## 数据需求Data Requirements

### 逻辑数据模型Logical Data Model

@import "./00_Diagram/03_COS_ERD.puml"

图C-3 食堂订餐系统版本1.0的局部数据模型

### 数据字典Data Dictionary

（略）

### 报表Reports

#### 订餐历史报表Ordered Meal History Report

（略）

### 数据集成、留存和销毁Data Integrity, Retention, and Disposal

1. DI-1： COS系统将自送餐日期起，为顾客的每个订单保留6个月
1. DI-2： COS系统将自菜单日期起，保留菜单1年

## 外部接口需求External Interface Requirements

### 用户界面User Interfaces

1. UI-1： 食堂预定系统界面显示应遵循[《Process Impact公司互联网应用用户界面标准2.0版》](REF-PI_IUIS)
1. UI-2： 系统为每个展现的网页提供一个帮助链接来说明如何使用该页面
1. UI-3： 页面将允许仅通过键盘即可完成导航和选择菜品，此外还可通过键盘与鼠标结合的方式完成

### 软件接口Software Interfaces

1. SI-1： 食堂库存系统
    1. SI-1.1： COS系统将通过编程接口将预订菜品的数量传递给食堂库存系统<!--与SI-2的描述结构不一致-->
    1. SI-1.2： COS系统将对食堂库存系统进行调取，确定所请求的菜品是否可用
    1. SI-1.3： 当食常库存系统通知COS系统某种特定的菜品不再可用时，COS系统将会从当天的菜单中移除该菜品
1. SI-2： 薪资系统： COS系统将通过编程接口与薪资系统进行通信以完成以下操作：
    1. SI-2.1： 允许顾客注册或注销薪资抵扣
    1. SI-2.2： 查询顾客是否注册了薪资抵扣
    1. SI-2.3： 查询顾客是否有资格注册薪资抵扣
    1. SI-2.4： 提交订餐支付请求
    1. SI-2.5： 由于顾客拒收或不满，或由于有未送达的送餐指令，因此要返还此前的费用

### 硬件接口Hardware Interfaces

尚未识别出硬件接口

### 通信接口Communications Interfaces

1. CI-1： COS系统将根据用户账户设置向顾客发送email或text message，以确认订单、价格以及送餐指令已被接受
1. CI-2： COS系纺将根据用户账户设置向顾客发送email或text message，以报告在订单或送餐中的任何问题

## 质量属性Quality Attributes

### 易用性需求Usability Requirements

1. USE-1： COS系统将允许顾客通过单次交互对之前预订的饭菜进行查找
1. USE-2： 95%的新用户首次尝试订餐能够成功

### 性能需求Performance Requirements

1. PER-1： 系统应容纳400个用户和100个并发用户（当地时间上午09:00至上午10:00的使用高峰时段、每个会话平均时长8分钟的情况下）
1. PER-2： 在20Mbps或更快速的互联网连接下，95%由COS系统生成的网页能在用户请求页面之后4秒钟内完成下载
1. PER-3： 系统在用户提交信息后，向用户显示确认信息平均不超过3秒钟，最多6秒钟

### 保密性需求Security Requirements

1. SEC-1： 所有含有财务信息或个人身份信息的网络事务将按照BR-33进行加密
1. SEC-2： 除查看菜单外，用户进行所有操作都需要登录到COS系统
1. SEC-3： 只有授权的菜单经理允许对菜单进行操作，按照BR-24
1. SEC-4： 系统只允许顾客查看自己下的订单

### 安全性需求Safety Requirements

1. SAF-1： 用户将能够看到任何一个菜品的全部成分列表，并对已知的导致0.5%北美人口过敏的成分进行高亮显示

### 可用性需求Availability Requirements

1. AVL-1： COS系统在上午05:00至当地时间午夜之间至少98%的时间可用，在当地时间午夜至上午05:00之间到少90%的时间可用（不包括计划内的维护时间段）

### 健壮性需求Robustness Requirements

1. ROB-1： 如果用户与COS系统间的连接在对新订单进行确认或终止之前断开，COS系统能够使用用户恢复未完成的订单并继续对该订单进行处理

## 附录A：分析模型Analysis Models

图C-4是一幅状态转换图，展现了可能的订餐订单状态以及所允许的状态转变

@import "./00_Diagram/02_COS_Order_SD.puml"
