---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第15章：通过原型减少风险Risk Reduction Through Prototyping:book:P264

<!-- slide -->
# :book:P264案例

1. 预想一个未来的软件系统并表述出它的需求是困难的
1. 评论一个东西比想象一个东西容易得多

<!-- slide -->

![COQ_ScopeCreep6W](./00_Image/COQ_ScopeCreep6W.png)

<!-- slide -->
# 本章重点和难点

1. 本章重点：
    1. 原型的定义及其目的
    1. 原型的三类属性：
        1. 范围： 实物模型与概念证明
        1. 未来用途： 抛弃型原型与演进型原型
        1. 形式： 纸上原型与电子原型
1. 本章难点：
    1. 原型的使用、评估、风险与成功因素

<!-- slide -->
# 使用广泛的可视化原型工具

1. **Adobe `PS` (PhotoShop)**
1. **Adobe `XD` (eXperience Design)**
1. **Bohemian Coding `Sketch`**
1. **Axure `Axure RP` (Rapid Prototyping)**
1. **`inVision` Studio**
1. **`Figma`**
1. **成都`Mockplus`**

<!-- slide -->
# 原型的定义及其动机Prototyping: What and Why:book:P265

1. 原型： 对新产品的部分、可能的或是初步的实现
1. 原型的主要目的：
    1. 明确、完成以及验证需求（承上）
    1. 便于与各方进行较无歧义的沟通（沟通）
    1. 探究设计的选择方案（启下）
    1. 创建一个可能演变成成品的部分系统（演进）
1. 原型的主要原因： 尽早解决不确定的问题
1. 创建原型的原则：
    1. 不必要为整个系统创建原型
    1. 对高风险的或不确定的部分创建原型
    1. 对有歧义的或不完整的部分创建原型

<!-- slide -->
# 三类原型属性

1. 范围Scope：
    1. **实物模型Mock-Up Prototype：** 重点关注用户体验
    1. **概念证明Proof-of-Concept Prototype：** 探究技术合理性
1. 未用用途Future Use：
    1. **一次性（可抛弃型）原型Throwaway Prototype：** 在产生反馈信息后会被抛弃
    1. **演进型原型Evolutionary Prototype：** 经一系列迭代后发展成为最终产品
1. 形式Form：
    1. **纸上原型Paper Prototype：** 画在纸、白板或草图（使用画图工具）
    1. **电子原型Electronic Prototype：** 由只针对部分解决方案的可工作软件组成

<!-- slide -->
# 实物模型与概念证明Mock-Ups and Proofs-of-Concept(POC):book:P266

1. 说到“软件原型”时，想到的通常是可能的UI实物模型
1. 实物模型： 也称为“水平模型（Horizontal Prototype），一般不具备实现行为（不包含或很少包含实际的功能实现），功能一般是硬编码、数据一般是模拟数据
    1. **重点关注UI：** 可以展示功能选项、用户界面的外观和感觉（颜色、布局、图形、控件）、导航结构等
    1. 不深入涉及架构的各个层次和详细的功能
    1. 探究预期系统的某些特定行为，帮助用户判断功能是否以合理的方式进行工作
    1. 使用抛弃型实物模型时，应重点关注 **概要性需求** 和 **工作流** ，而不是确切外观
1. 概念证明： 也称为“垂直模型”（Vertical Prototype），在Agile中也称为spike，在所有技术服务层次上从UI实现一部分功能
    1. 运行方式与真实系统相似（触及系统实现的所有层次）
    1. 使用与产品相似操作环境的产品工具构建POC

<!-- slide -->
# 抛弃型原型和演进型原型Throwaway and Evolutionary Prototypes:book:P267

1. 抛弃型原型： 解释一些问题、解决不确定性以及改进需求
    1. 应以快速、低成本的方式创建抛弃型原型
    1. 创建抛弃型原型时，会忽略成品软件构建技术，更注重快速实现和快速修改
    1. 最适合抛弃型原型的情形： 需求不确定、有歧义、不完整、含糊、从独立需求难以想象出的未来系统
    1. 线框图wireframe： 对话图也是一种wireframe
1. 演进型原型： 为增量构建产品提供一个稳固的架构基础，Agile是一个演进型模型
    1. 构建演进型原型一开始就需考虑健壮性，写出产品级质量的代码，重视软件架构和稳健的设计原则

<!-- slide -->

![httpatomoreillycomsourcemspimages1792318](./00_Image/httpatomoreillycomsourcemspimages1792318.png)

<!-- slide -->
# 纸上原型和电子原型Paper and Electronic Prototypes:book:P270

1. 纸上原型/低保真原型Low-Fidelity Prototype：帮助探究部分外观、一种低成本、迅速、低技术难度的原型，纸上原型与故事看板Storyboard类似
1. 用低保真原型探究功能和流程，用高保真原型来确定具体的样式和外观
1. 无论是哪一种原型，BA都应避免过早涉及 **过于细节化** 的UI设计

<!-- slide -->
# 原型与其属性的关系:book:P266

1. 每一个原型都将民现三种属性的综合物征，比如，纸上画的屏幕图，属于纸上、可抛弃的实物模型
1. 有些特定组合并不合理，存在属性排斥的情况，比如，不存在演进型的纸上概念证明模型

<!-- slide -->
# 原型的使用Working with Prototypes:book:P271

![httpatomoreillycomsourcemspimages1792319](./00_Image/httpatomoreillycomsourcemspimages1792319.png)

## 相比从UCS直接到整个UI完成后可能导致大量返工，逐步求精的方法更为可取

<!-- slide -->

![httpatomoreillycomsourcemspimages1792320](./00_Image/httpatomoreillycomsourcemspimages1792320.png)

<!-- slide -->

![httpatomoreillycomsourcemspimages1792321](./00_Image/httpatomoreillycomsourcemspimages1792321.png)

<!-- slide -->

![httpatomoreillycomsourcemspimages1792322.png](./00_Image/httpatomoreillycomsourcemspimages1792322.png.jpg)

<!-- slide -->
# 原型的评估Prototype Evaluation:book:P274

1. 原型评估与易用性测试有关
1. 合适的人从恰当的角度评估原型
    1. 评估人员： 评估人员来自多个用户群，包括有经验的、也包括缺乏经验的
    1. 评估方法： 创建一些脚本帮助用户完成一系列操作和回答一些特定的问题
1. 记录下原型评估中获得的信息，以提练需求

<!-- slide -->
# 原型的风险Risks of Prototyping:book:P275

1. 原型可以降低风险，但原型本身也有风险
1. 原型的风险：
    1. 以原型代替产品发布
    1. 过于关注原型细节
    1. 意象的性能预期
    1. 对原型投入过多

<!-- slide -->
# 原型成功的因素Prototyping Success Factors:book:P277

1. 预留资源： 项目计划中为原型预留资源
1. 明确目标： 明确原型的目的和最终产出， 是抛弃还是演进？
1. 多个计划： 做好开发多个原型的计划
1. 快速、低成本： 创建可抛弃型原型应尽可能快、成本低
1. 降低复杂度： 创建可抛弃型原型避免包含输入验证、防护性编码或技术文档
1. 针对性创建： 不要对已明确的需求创建原型，除非需探究其他设计方案
1. 合理数据： 在原型的屏幕显示和报表中使用合理的数据
1. 有限作用： 不要指望原型替代SRS书面需求

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
