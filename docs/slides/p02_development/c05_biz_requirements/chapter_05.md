---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# Part 02: Requirements Development:book:P65

<!-- slide -->
# 第05章：建立业务需求Establishing the Business Requirements:book:P67

<!-- slide -->
# :book:P67案例、P69案例

1. 在挖掘并记录这个特性的功能需求之前，她仍需要一个 **客观的分析** 来判断是否应该把该特性添加到范围之中
1. 根据分析，Karon判断该特性超出了项目的范围
    1. **它无助于减少客服代表平均通话时间这个业务目标** ==> 根本原因
    1. 开发起来也没有那么简单 ==> 其他原因

## :book:P69确保愿景可以解决问题

<!-- slide -->
# 本章重点和难点

1. 本章重点：
    1. 业务需求是什么？业务需求的重要性
    1. 关联图（环境图）、生态系统图、特性图、事件列表是什么？

<!-- slide -->
# 业务需求和业务目标:book:P67

1. 业务需求代表的是需求链的顶部，定义 **解决方案的愿景** 和实现 **该解决方案的项目范围**
1. 用户需求和功能需求必须与业务需求建立的背景和目标保持一致
1. 任何无助于项目达成业务目标的需求都不宜实现

## 如果对项目的业务目标缺乏共同的理解，干系人永远无法就需求达成一致意见

## VSD包含项目的业务需求

<!-- slide -->
# 定义业务需求:book:P67

1. “业务需求”指的是一组信息，这组信息描述的是“需要”，在此需要的指导下，一个或多个项目交付一个解决方案和符合预期的最终业务成果
1. 业务需求一般包括： **业务机会**、**业务目标**、**成功标准**和一个 **愿景声明**

<!-- slide -->
# 确定预期业务收益:book:P68

## 如果不清楚项目能为业务增加什么价值，就不要启动任何项目

1. 为业务目标设置 **可度量的** 目标，然后定义指标，以便衡量是否在实现这些目标的正确轨道上。业务收益必须体现对项目发起人和产品客户的真正价值，**增加收入** 或 **降低成本**
1. 业务需求可能来自 **出资方** 、 **企业高管** 、 **市场部经理** 或 **产品规划者**。业务分析师应能够确保有合适的干系人设置业务需求和引导获取活动、优先级排序和解决冲突

<!-- slide -->
# 愿景和项目范围:book:P68

## 业务需求的两个核心元素：愿景和范围

1. 愿景： 简要描述最终产品将要达成什么业务目标，即描述的是产品 **大约是什么** 并且 **最终（大约）变成什么**
1. 范围： 明确当前项目或开发迭代应强调最终产品愿景的哪些部分，即描述的项目（开发项目产出的产品） **内外的边界**

## :point_right:愿景保证对最终目标心里有数，范围确保讨论集中于当前项目或迭代中的同一件事

<!-- slide -->
# 相对静态的愿景和相对动态的范围:book:P69

![httpatomoreillycomsourcemspimages1792269](./00_Images/httpatomoreillycomsourcemspimages1792269.png)

<!-- slide -->
# 业务需求冲突

## 各方干系人的目标有时是一致的，有时可能存在冲突，应该集中于首要干系人交付最大的价值

<!-- slide -->
# 愿景和范围文档:book:P71

1. VSD将业务需求集合成为一个独立的交付物，为后续的开发工作奠定基础，**VSD的所有者（最重要的目标读者）是项目的执行发起人、出资方或某个类似的角色**
1. MRD与VSD相似，但可能更侧重于（大量篇幅描写）目标市场细分和关乎商业（商业模式）成败方面的问题
1. VSD在高层面上定义范围，团队定义的每个版本基线（包括其他文档）体现的是范围的细节
1. 增强型项目可能将其范围声明归入项目的SRS，**不一定需要再创建一个独立的VSD**

<!-- slide -->
# VSD模板

## :point_right:不管使用哪种模板，目的都是满足具体项目的具体需要

1. 模板的作用：
    1. 确保每个项目以一致的方法组织信息
    1. 确保项目成员以一致的理解组织信息
    1. 有助于记住可能被忽略的信息
1. 使用模板的建议：
    1. 模板内容丰富，包含重要的类别
    1. 不存在于当前项目的章节，应明确标注说明情况

<!-- slide -->
# 建议的VSD模板

![httpatomoreillycomsourcemspimages1792271](./00_Images/httpatomoreillycomsourcemspimages1792271.png)

<!-- slide -->
# 业务目标模型:book:P75

![httpatomoreillycomsourcemspimages1792273](./00_Images/httpatomoreillycomsourcemspimages1792273.png)

<!-- slide -->
# VSD模板-1.4成功指标

## :warning::exclamation:确保度量的是业务要点，而不仅仅是因为那部分容易度量（以实现业务目标为标准）

<!-- slide -->
# 范围表示工具:book:P80

1. :point_right:**关联图（Context Diagram）**、**生态系统图（Ecosystem Map）**、**特性树（Feature Tree）**、**事件列表（Event List）** 是最常用的 **范围可视化模型**。另外，还可以通过用例图（Use Case）描述用例与角色之间的边界范围
1. :point_right:不需要创建所有模型，只需要考虑哪个模型可以为项目提供最有用的见解
1. :point_right:使用模型工具，目的是在干系人间培养清晰和准确的沟通机制

<!-- slide -->
# 关联图（环境图）

1. 关联图通过 **某一接口** 与系统相连的 **外部实体（端点）** 之间的关系直观展示系统和周围事物之间的边界和连接
1. 关联图还包括数据、控制以及系统与端点之间的物料流转
1. 关联图是按结构化分析原则制定的 **数据流图** 的 **最高抽象层**，适用于所有项目
1. 关联图 **不提供** 内部对象、内部流程或内部数据的可视化表示

<!-- slide -->
# 关联图:book:P82

![httpatomoreillycomsourcemspimages1792274](./00_Images/httpatomoreillycomsourcemspimages1792274.png)

<!-- slide -->
# 生态系统图

1. 生态系统图展示了所有与系统利益相关的 **系统** 相互作用以及这些互动的本质
1. 生态系统图与关联图的区别在于：生态系统图展示的是正在开发的系统与其他系统的关系，**包括没有直接接口的系统**
1. 当项目不再影响任何其他数据时，就能发现参与解决方案的那个系统的范围边界
1. 线表示系统之间的接口（双向数据流向），带箭头的线显示主要的数据流向

<!-- slide -->
# 生态系统图:book:P83

![httpatomoreillycomsourcemspimages1792275](./00_Images/httpatomoreillycomsourcemspimages1792275.png)

<!-- slide -->
# 特性树:book:83

1. 特性树展示按 **逻辑分组** 的产品特性，并将每种特性逐级分解到下一级
1. 特性树显示 **三个层次** 的特性，通常称为一级（L1）、二级（L2）和三级（L3）
1. 做版本或迭代规划时，可以选择一组明确的特性和子特性定义其范围

<!-- slide -->
# 特性树

![httpatomoreillycomsourcemspimages1792276](./00_Images/httpatomoreillycomsourcemspimages1792276.png)

<!-- slide -->
# 事件列表:book:84

## :sparkle:事件列表确定了可能引发系统行为的外部事件:sparkle:

例如：可能被用户触发的业务事件、由时间触发的事件或从外部组件接收到的信号事件等

1. 事件列表只列出事件的名称
1. 每一项声明 **事件的来源** 并标识出 **事件的动作**
1. 可以将事件分配到特定的产品版本或开发迭代中

<!-- slide -->
# 事件列表:book:P85

![httpatomoreillycomsourcemspimages1792277](./00_Images/httpatomoreillycomsourcemspimages1792277.png)

<!-- slide -->
# 事件列表与关联图、生态系统图

1. **事件列表是对关联图和生态系统图的补充**
1. 关联图和生态系统描述涉及的外部角色和系统，而事件列表打描述这些角色和系统在特定系统中可能触发的行为
1. 为了准确和完整，可以使用关联图和生态系统图来检查事件列表（或相反方向）

<!-- slide -->
# 聚焦于范围:book:P85

1. 对待变更： 范围变更并不是坏事，它能够帮助驾驭项目，满足客户不断变化的需求
1. 评估基准： VSD的信息可以作为评估是否把提出的需求加入该项目的基准
1. 迭代反馈： 业务需求和用户需求之间存在一个反馈循环，要求不断更新VSD，在文档被确认为基线后控制变更
1. 记录事件： 记录需求被拒绝的理由，重现机制

<!-- slide -->
# 使用业务目标来做范围决策

1. 确定早期版本特性： 确定哪些特性或用户需求能够给业务目标带来最多价值，将其安排到早期版本中
1. 评估需求变更： 当干系人想添加某项功能时，考虑他建议的改变是否有且于达成业务目标 ==> :book:P67案例
1. 评估变更影响： 评估范围变更的影响

<!-- slide -->
# 使用业务目标来确定完成:book:P87

1. :point_right:成功指标显示很可能达成了业务目标，（产品开发）项目就可以结束了
1. :warning:模糊的商业目标肯定出现一个开放式的项目，无法知道什么时候才算完成

<!-- slide -->
# :ok:End of This Chapter:ok:
