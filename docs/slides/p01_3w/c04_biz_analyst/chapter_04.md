---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---


<!-- slide -->
# 第04章：业务分析师The Business Analyst:book:P53

<!-- slide -->
# :book:P53案例

:point_right:每个软件项目，都有人 **显式** 或 **隐式** 地扮演着BA的角色，BA **获取** 和 **分析** **他人** 的观点，将信息转换（**建模**）为SRS，并与其他干系人 **沟通** 和 **交流** 这些信息

<!-- slide -->
# 本章重点和难点

1. BA的职责
1. BA的来源

<!-- slide -->
# :book:P54图4-1

![httpatomoreillycomsourcemspimages1792267](./00_Images/httpatomoreillycomsourcemspimages1792267.png)

<!-- slide -->
# PM和BA:book:P55

:point_right:如果项目中即有PM又有BA，通常PM **主抓外部市场和用户请求**，再由BA将其 **转换为功能需求**

:point_right:项目经理（Project Manager）和产品经理（Product Manager）的缩写均为PM，在无特殊说明下，PM均指产品经理，当指代项目经理时直接使用“项目经理”

<!-- slide -->
# BA的职责

1. 定义业务需求
1. 规划需求方法
1. 确定干系人和用户类别
1. 获取、分析、记录需求
1. 主导需求验证
1. 帮助推动需求优先级排序
1. 管理需求

<!-- slide -->
# BA的建模技巧:book:P58

1. 与客户沟通：有些模型适合与客户的沟通
1. 与开发沟通：有些模型适合与开发人员的沟通
1. 仅用于辅助：有些模型用于纯分析以帮助BA改进需求

<!-- slide -->
# 培养BA（BA的来源）

1. 用户：优点=>理解业务，缺点=>与技术人员沟通困难
1. **主题专家：** 优点=>领域专家，缺点=>易刚腹自用
1. 菜鸟：优点=>一张白纸，缺点=>缺乏经验和知识
1. **开发人员：** 优点=>技术强，缺点=>不熟悉业务，不喜与人沟通，不喜于写文档
1. 测试人员：优点=>分析和挑问题思维，缺点=>技术和沟通能力不强
1. 项目经理：优点=>沟通、组织、写作能力强，缺点=>分析、建模、访谈能力差

<!-- slide -->
# 敏捷项目中的BA

在使用敏捷开发方法的项目中，BA的职责仍然需要有人完成，但可能并没有BA的头衔

<!-- slide -->
# Thank You

## :ok:End of This Chapter && This Part:ok:
