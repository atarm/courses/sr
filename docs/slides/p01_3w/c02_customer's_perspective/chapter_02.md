---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第02章：从客户角度审视需求Requirements From the Customer's Perspective:book:P22

<!-- slide -->
# :book:P22案例

<!-- slide -->
# 本章重点和难点

1. 本章重点：
    1. 期望落差
    1. 需求基线

<!-- slide -->
# 减少期望落差:book:P24图2-1

![httpatomoreillycomsourcemspimages1792262](./00_Images/httpatomoreillycomsourcemspimages1792262.png)

<!-- slide -->
# 干系人:book:P25

:point_right:所有干系人共享的同一个目标：构建一个既实现业务价值又可以使所有干系人受益的产品

1. 项目组
1. 开发组织
1. 开发组织之外

<!-- slide -->
# 客户

客户是干系人的一个子集，是能够直接或间接从产品中获益的个人或组织

1. 终端用户： 直接用户、间接用户
1. 客户代理： 产品经理、营销人员

<!-- slide -->
# 软件客户的需求权利和需求责任:book:P27

1. 开发公司内部系统、合同型项目或定制系统时，适用于实际客户
1. 开发大众市场产品，适用于客户代表

<!-- slide -->
# 需求基线

## 需求基线是一组需求，在评审和确认后作为后续开发的基础

1. 客户承认需求描述了他们的需要
1. 开发人员承认理解需求并且认为是可实现的
1. 测试人员承认需求是可验证的
1. 管理层承认需求可以达成他们的业务目标

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
