---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# Part 04:  Requirements Management:book:P401

<!-- slide -->
# 第27章：需求管理实践Requirements Management Practices:book:P403

<!-- slide -->
# 本章重点和难点

1. 本章重点：
    1. 需求管理的各活动
    1. 需求基线

<!-- slide -->
# 案例:book:P403

1. 版本控制？
1. 团队协作？

## :exclamation:单个需求和需求集合的版本控制是需求管理的核心活动和核心基础之一

<!-- slide -->
# 本章重点内容

1. 需求管理的定义和核心活动
1. 需求基线
1. 需求版本控制

<!-- slide -->
# 需求管理

1. 需求管理的定义： 保持需求在项目过程中的完整性、准确性和时效性的所有活动
1. 需求管理的核心活动： 版本控制、变更控制、需求状态跟踪和需求跟踪

<!-- slide -->
# 需求管理流程Requirements Management Process:book:P403图27-1

![httpatomoreillycomsourcemspimages1792356](./00_Image/httpatomoreillycomsourcemspimages1792356.png)

<!-- slide -->
# 建立团队的需求管理流程

1. 角色清晰： 流程描述应确定负责每个需求管理活动的团队角色
1. 过程清晰： 流程描述应指明谁有权修改需求管理过程、如何处理异常以及遇到阻碍时如何升级

<!-- slide -->
# 需求基线The Requirements Baseline:book:P405

1. 需求基线的定义： 需求开发的交付物经评审且核准后形成的已定义的集合或其子集组成需求基线
1. 需求基线的变更管理： 需求基线形成后置于配置管理（或变更管理）之下，之后变更须通过项目定义好的变更控制流程进行
1. 需求基线的管理工具： 在RM工具中，应保存好能快速识别特定基线需求的标识并管理基线的变更

<!-- slide -->
# 需求版本控制Requirements Version Control:book:P405

1. 版本控制： 唯一性地标识一个条目的不同版本，同时，保存变更历史
1. 信息同步： 需求变更必须清楚记录并同步给每一个受影响的人
1. 权限控制： 只允许指定的人能更新需求，且更新后确保版本号更新
1. 信息记录： 每个需求应当包含修订历史以记录每次的修改内容、修改的时间、修改的作者以及修改的原因

<!-- slide -->
# 需求属性Requirement Attributes:book:P407

1. 将每个需求当作一个带有支持信息或相关属性的对象，使其有别于其他需求，这些属性为每个需求构建了一个上下文和背景
1. 可考虑的需求属性列表：创建日期、当前版本号、需求作者、优先级、状态、需求来源、需求原由、已发布的版本号或需求分配的迭代信息、干系人、验证方法或验收条件

## 需求属性应该记录在哪里:question:

<!-- slide -->
# 跟踪需求状态Tracking Requirements Status:book:P408

1. 跟踪每个功能需求的状态能提供一个更精确的项目进度表
1. 将需求分成状态类别（:book:P409表27-1）远比试图监控完成比例（:book:P408 && :book:P409的90%）更有意义
1. 以可视化的方式（:book:P410图27-2）监控需求集合的状态

<!-- slide -->
# 解决需求问题Resolving Requirements Issues:book:P410

1. 未解决的问题容易被忽视，因此，需要将这些问题记录在问题跟踪工具供所有干系人访问
1. 常见的需求问题类型:book:P411表27-2

<!-- slide -->
# 度量需求投入Measuring Requirements Effort:book:P411

1. 投入计划： 在需求开发过程生成的项目计划中应包含需求管理活动的任务和资源
1. 记录投入： 跟踪并记录需求工程的投入，可以评估投入是否合理并相应地调整未来的计划
1. 改进投入： 将BA所花的时间和项目其他参与者分开跟踪，以便帮助计划未来的项目需要投入多少BA的资源

<!-- slide -->
# 敏捷项目的需求管理Managing Requirements on Agile Projects:book:P412

<!-- slide -->
# 为什么要管理需求Why Manage Requirements:book:P414

1. 避免浪费： 有助于确保在需求开发上的投入不会浪费
1. 降低落差： 通过保证所有项目干系人了解开发过程的需求状态以降低期望落差
1. 心中有数： 知道去哪里、如何去以及大约何时能到达目的地

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
