---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第24章：业务流程自动化项目Business Process Automation Projects:book:P372

<!-- slide -->
# 本章重点和难点

1. 本章重点： 自动化的目的
1. 本章难点： 业务流程术语的区别和联系

<!-- slide -->
# 业务流程自动化项目是什么？

1. 大多数企业的IT项目或多或少都与业务流程自动化相关
1. 自动化的目的： 通过软件代替人工实施耗时的、重复的工作

<!-- slide -->
# 业务流程建模:book:P372

1. 流程建模： 通过识别用户要求系统完成的任务，BA定义执行这些任务所需要的功能性需求
    1. as-is流程： 业务的当前运行流程
    1. to-be流程： 业务的预期运行流程
1. 业务流程的术语：
    1. 业务流程分析（Business Process Analysis, BPA）
    1. 业务流程重组（Business Process Reengineering, BPR）
    1. 业务流程改进（Business Process Improvement, BPI）
    1. 业务流程管理（Business Process Management, BPM）
1. 业务流程模型和标记法（Business Process Model and Notation, BPMN）

<!-- slide -->
# 基于当前业务流程推导出需求

1. 获取业务目标
1. 从组织结构图找出用户群
1. 识别用户群参与的相关业务流程
1. 使用流程图、活动图或泳道图对as-is流程进行记录
1. 分析as-is流程，确定自动化对改进带来的机遇，发现to-be流程
1. 与合适的干系人分析出as-is和to-be流程对应的软件需求
1. 回溯发现可能被遗漏的需求
1. 识别新系统中存在的过程差异
1. 迭代修正

<!-- slide -->
# 设计未来的业务流程

1. 某些情况下，希望通过构建新系统驱动企业的BPI或BPR
1. **事实上**，BPR涉及企业文化的变革和用户教育，这并不是软件系统能实现的
1. 正常情况下，设计新的业务流程，然后评估软件系统所需要进行的调整

<!-- slide -->
# 业务绩效指标建模:book:P375

## KPI决定优先级

<!-- slide -->
# 业务流程自动化项目的良好实践:book:P376表24-1

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
