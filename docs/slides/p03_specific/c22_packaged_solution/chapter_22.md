---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第22章：软件包方案项目Packaged Solution Projects:book:P359

<!-- slide -->
# 本章重点和难点

1. 本章重点： 软件包项目的4种实施类型
1. 本章难点： 理解软件包项目的优缺点、自制与外购的优缺点

<!-- slide -->
# 自制vs外购

1. 外购的优点
    1. 节省人力成本 ==> 采方的人力单价高于供方的人力单价
    1. 弥补经验不足 ==> 供方的经验积累优于采方的经验积累
1. 外购的缺点
    1. 灵活度底： 特别是软件包方案
    1. 可控性差： 受限于供方的能力，以合同条款为准

## 外包项目和软件包项目有什么异同:question:

<!-- slide -->
# 商业现货COTS

>Commercial off-the-shelf or commercially available off-the-shelf (COTS) products are packaged solutions which are then adapted to satisfy the needs of the purchasing organization, rather than the commissioning of custom-made, or bespoke, solutions.
>
>---- [wikipedia: Commercial off-the-shelf](https://en.wikipedia.org/wiki/Commercial_off-the-shelf)

<!-- slide -->
# 开箱即用Out-Of-The-Box

>An out-of-the-box feature or functionality (also called OOTB or off the shelf), particularly in software, is a feature or functionality of a product that works immediately after or even without any special installation without any configuration or modification. It also means that it is available for all users by default, and are not required to pay additionally to use those features, or needs to be configured.
>
>---- [wikipedia: Out of the box (feature)](https://en.wikipedia.org/wiki/Out_of_the_box_(feature))

<!-- slide -->
# COTS与Requirements

1. 需求仍然需要： 无论采购软件包，还是实施云端方案，要满足新项目方案整体或局部对软件的需要，仍然离不开需求
1. 需求评估候选： 根据需求对候选方案进行评估，选出最合适的软件包，此后，根据需求调整软件包以满足组织的需要

<!-- slide -->
# COTS:book:P359图22-1

![httpatomoreillycomsourcemspimages1792343](./00_Image/httpatomoreillycomsourcemspimages1792343.png)

<!-- slide -->
# 进行软件包方案选型的需求:book:P360

1. 灵活度低： 相比定制开发，COTS能提供的灵活度更低，需要了解哪些能力没有商量余地、哪些可以调整
1. 识别高抽象层（粗粒度）软件需求： 至少需要识别出高抽象层（粗粒度）的软件需求
1. 详细度不定： COTS选型所需要的文档详细程度和工作量，取决于软件包的预期成本、评估时间表以及候选方案数量等

<!-- slide -->
# 开发用户需求

1. 重点关注用户需求层面： COTS采购需求工作重点在用户需求层面，UC和US可以很好地达到此目的
1. 用户需求优先级排序： 没有哪个软件包方案完全符合识别出的全部用户需求

<!-- slide -->
# 考虑业务规则

## 识别出COTS产品必须满足的相关业务规则

<!-- slide -->
# 识别数据需求

1. 找出数据需求模型与软件包的数据模型之间的重要差异
1. 明确指定COTS方案必须生成的报表

<!-- slide -->
# 定义质量属性需求

1. 性能Performance
1. 易用性Usability
1. 可修改性Modifiability
1. 互操作性Interoperability
1. 完整性Integrity
1. 安全性Security

<!-- slide -->
# 评估方案

1. 设定需求权重
1. 根据COTS与需求的契合度，为每个候选包打分值
1. 计算每个候选的加权分值
1. 综合成本投入、厂商经验和可靠度、厂商支持、技术架构要求和业务约束契合度等其他因素综合评估

<!-- slide -->
# 评估矩阵:book:P363图22-2

![httpatomoreillycomsourcemspimages1792344](./00_Image/httpatomoreillycomsourcemspimages1792344.png)

<!-- slide -->
# 实施软件包方案的需求:book:P364

1. 为了使软件包方案正式投入使用，从开箱即用到软件扩展可能有一系列工作待完成
1. 采购COTS方案的优点之一，是能提供先前未曾考虑到的有价值的能力

<!-- slide -->
# 软件包方案实施工作的范围:book:P364图22-3&&表22-1

![httpatomoreillycomsourcemspimages1792345](./00_Image/httpatomoreillycomsourcemspimages1792345.png)

<!-- slide -->
# 软件包方案实施工作

1. 配置需求： 成功的COTS实施大多离不开配置
1. 集成需求： 除非单机使用，否则需要将其集成到应用环境中
1. 扩展需求： 针对需求与COTS能力的差异，有时需要定制扩展
1. 数据需求： 将组织的数据字典映射到COTS的数据模型

<!-- slide -->
# 业务流程变更

不同于为适应现有或计划的流程而设计构建的开发项目，组织可能需要准备调整自身的业务过程 **以适配软件包的工作流程能力和限制**

<!-- slide -->
# 软件包方案的常见挑战:book:P366

1. 采方侧造成的挑战
    1. 方案预期过高
    1. 评估标准过多
    1. 候选方案过多
    1. 用户拒绝使用
1. 供方侧造成的挑战
    1. 谎报功能过多
    1. 软件成本过高
    1. 功能定制困难

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
